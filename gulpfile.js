'use strict';

var gulp = require('gulp'),
sassLint = require('gulp-sass-lint'),
sass = require('gulp-sass'),
rename = require('gulp-rename'),
cleanCSS = require('gulp-clean-css'),
sourcemaps = require('gulp-sourcemaps'),
browserSync = require('browser-sync').create();

gulp.task('sass', function () {
    return gulp.src('./app/sass/**/*.scss')
    //.pipe(sassLint())
    //.pipe(sassLint.format())
    //.pipe(sassLint.failOnError())
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./app/css'));
});

gulp.task('sass-pages', function () {
    return gulp.src('./app/sass/pages/*.scss')
    //.pipe(sassLint())
    //.pipe(sassLint.format())
    //.pipe(sassLint.failOnError())
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./app/css/pages'));
});

gulp.task('sass:watch', function () {
    gulp.watch(['./app/sass/**/*.scss','!./app/sass/pages/*.scss'], ['sass']);
    gulp.watch(['./app/sass/pages/*.scss'], ['sass-pages']);
});

gulp.task('minify-css', function() {
    return gulp.src('./app/css/*.css')
    .pipe(sourcemaps.init())
    .pipe(cleanCSS())
    .pipe(sourcemaps.write())
    .pipe(rename({ suffix: '.min' }))
    .pipe(gulp.dest('./dist'));
});

gulp.task('serve', function() {
    browserSync.init({
        server: {
            baseDir: "./app"
        }
    });
    gulp.watch("app/css/*.css").on('change', browserSync.reload);
    gulp.watch("app/*.html").on('change', browserSync.reload);
});

gulp.task('default', ['serve']);
gulp.task('dist', ['minify-css']);
